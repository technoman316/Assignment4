package assignment4;

public class Room {
		
		/**
		 * @author cjp2790
		 * 8/20/2014
		 * This is the main class file for this assignment.
		 */
		
		private String wall;
		private String floor;
		private String window;
		public String toString() {
			return "The floor material is "+floor+", the color is "+wall+", and there are "+window+" windows.";
		}
		
		/**
		 * Constructs a new room that has colored walls and contains a material for flooring and a certain number of windows.
		 */
		public Room() {
			this.wall = "color";
			this.floor = "wood";
			this.window = "window";
		}

	   /**
		* Constructs a new room that has walls, a material for flooring, and a number of windows based on the arguments.
		* @param wall set wall
		* @param floor set floor
		* @param window set window
		*/
		
		public Room (String wall, String floor, String window){
			this.wall = wall;
			this.floor = floor;
			this.window = window;
		}
		
		/**
		 * Gets wall
		 * @return The color of the wall
		 */
		
		public String getWall(){
			return wall;
		}
		
		/**
		 * Sets wall
		 * @param wall Sets the color of the wall
		 */
		
		public void setWall (String wall){
			this.wall = wall;
		}
		
		/**
		 * Gets floor
		 * @return The material of the floor
		 */
		
		public String getFloor(){
			return floor;
		}
		
		/**
		 * Sets floor
		 * @param floor Sets the material of the floor
		 */
		
		public void setFloor (String floor){
			this.floor = floor;
		}
		
		/**
		 * Gets window
		 * @return The number of windows
		 */
		
		public String getWindow(){
			return window;
		}
		
		/**
		 * Sets window
		 * @param window Sets the number of the windows
		 */
		
		public void setWindow (String window){
			this.window = window;
		}
}