package assignment4;

public class RoomDemo {

	public static void main(String[] args) {
		/**
		 * @author cjp2790
		 * 8/22/2014
		 * This is the resource to accompany Room.java
		 */

		/**
		 * Creates RoomA with colored walls and then prints the output
		 */
		
		Room RoomA = new Room("yellow","wood","1");
		System.out.println(RoomA.toString());
		
		/**
		 * Creates RoomB with colored walls and then prints the output
		 */
		
		Room RoomB = new Room("purple","tile","no");
		System.out.println(RoomB.toString());
		
		/**
		 * Creates RoomC with colored walls and then prints the output
		 */
		
		Room RoomC = new Room("white","carpet","3");
		System.out.println(RoomC.toString());
	}
}
